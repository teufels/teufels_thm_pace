########################################
## INCLUDES FOR STAGING && PRODUCTION ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:teufels_thm_pace/Configuration/TypoScript/Setup/Production" extensions="txt">

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[globalString = ENV:HTTP_HOST=development.*]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:teufels_thm_pace/Configuration/TypoScript/Setup/Development" extensions="txt">
[global]